package seaFightGame

fun main() {
    val game = Game()
    game.start()
}

class Game {
    private val player1: Player
    private val player2: Player

    init {
        print("\n ~~~~ Введи имя первого игрока: ~~~~\n")
        val name1 = readlnOrNull() ?: ""
        player1 = if (name1.isNotEmpty()) Player(name1) else Player("Игрок 1")

        print("\n\n ~~~~ Введи имя второго игрока: ~~~~\n")
        val name2 = readlnOrNull() ?: ""
        player2 = if (name2.isNotEmpty()) Player(name2) else Player("Игрок 2")
    }

    private fun move(player: Player, coordinate: Array<Int>): Boolean {
        var hitTarget = false
        val column = coordinate[0]
        val line = coordinate[1]
        if (!player.gameBoard.checkShipPlace(column, line)) {
            player.gameBoard.map[line - 1][column] = 0
            val shipsToRemove = mutableListOf<Int>()
            for ((index, ship) in player.ships.withIndex()) {
                if (ship[0] == line - 1 && ship[1] == column) {
                    shipsToRemove.add(index)
                }
            }
            for (index in shipsToRemove.reversed()) {
                player.ships.removeAt(index)
            }
            hitTarget = true
        }
        return hitTarget
    }

    fun start() {
        var currentPlayer = player1
        var otherPlayer = player2

        while (otherPlayer.ships.isNotEmpty()) {
            var moveResult: Boolean

            do {
                moveResult = move(otherPlayer, currentPlayer.makeMove())
                if (moveResult) {
                    println("\n ~~~~ Попадание! ~~~~\n")
                    if (otherPlayer.ships.isEmpty()) {
                        println("\n\n ~~~~ УРА!!! ПОБЕДИТЕЛЬ: ${currentPlayer.name} !!! ~~~~ \n\n")
                        return
                    }
                } else {
                    println("\n ~~~~ Промах! ~~~~\n")
                }
            } while (moveResult)

            if (currentPlayer == player1) {
                currentPlayer = player2
                otherPlayer = player1
            } else {
                currentPlayer = player1
                otherPlayer = player2
            }
        }
    }
}

