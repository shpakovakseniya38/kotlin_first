package seaFightGame

class Board {
    var map = Array(4) { Array(4) { 0 } }

    fun printMap() {
        println("  A B C D")
        for (i in map.indices) {
            print("${i+1} ")
            for (j in map[i].indices) {
                if (map[i][j] == 0) print("- ") else print("+ ")
            }
            println()
        }
    }

    fun getColumnCoordinate(): Int {
        var column: Int = -1
        while (column < 0) {
            print("\nВведи координату столбца - букву A, B, C или D. \n")
            val input = readlnOrNull() ?: ""
            column = if (input.isNotEmpty()) {
                when (input) {
                    "A" -> 0
                    "B" -> 1
                    "C" -> 2
                    "D" -> 3
                    else -> {
                        print("\nНеправильная координата.\n")
                        -1
                    }
                }
            } else {
                print("\nНеправильная координата.\n")
                -1
            }
        }
        return column
    }

    fun getLineCoordinate(): Int {
        var line: Int = -1
        while (line < 0) {
            print("\nВведи координату строки - цифру 1, 2, 3 или 4. \n")
            val input = readlnOrNull() ?: ""
            if (input.isNotEmpty()) {
                line = input.toIntOrNull() ?: -1
                if (line > 4) {
                    print("\nНеправильная координата.\n")
                    line = -1
                }
            } else {
                print("\nНеправильная координата.\n")
                line = -1
            }
        }
        return line
    }

    fun getFieldCoordinates(): Array<Int> {
        val column = getColumnCoordinate()
        val line = getLineCoordinate()
        return arrayOf(line, column)
    }
    fun checkShipPlace(column: Int, line: Int): Boolean {
        return map[line - 1][column] == 0
    }

    fun checkSecondCoordinates(firstField: Array<Int>, secondField: Array<Int>): Boolean {
        val prevColumn = firstField[1]
        val prevLine = firstField[0] - 1
        val newColumn = secondField[1]
        val newLine = secondField[0] - 1

        if ((newColumn == prevColumn && (newLine == prevLine - 1 || newLine == prevLine + 1)) ||
            (newLine == prevLine && (newColumn == prevColumn - 1 || newColumn == prevColumn + 1))) {
            if (newColumn in 0..3 && newLine in 0..3) return true
        }
        return false
    }
}



