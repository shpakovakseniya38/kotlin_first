package seaFightGame

class Player(var name: String) {
    var gameBoard: Board = Board()
    var ships: MutableList<MutableList<Int>> = mutableListOf(mutableListOf())

    init {
        fillGameBoard()
    }

    private fun fillOneField(coordinates: Array<Int>) {
        val column = coordinates[1]
        val line = coordinates[0]
        gameBoard.map[line - 1][column] = 1
        if (ships[0].isEmpty()) {
            ships[0] = mutableListOf(line - 1, column)
        } else {
            ships.add(mutableListOf(line - 1, column))
        }
        println("\n Теперь твоя карта выглядит так: \n")
        gameBoard.printMap()
        println()
    }

    private fun fillGameBoard() {
        println("\n ~~~~  Привет, ${this.name}! ~~~~\n У тебя есть корабли: \n - однопалубный - 2 шт.\n - двухпалубный - 1 шт.")
        var coordinates: Array<Int>
        var isFilled: Boolean

        for (i in 1..2) {
            isFilled = false
            println("\n ~~~~ Размести на карте $i-й однопалубный корабль. ~~~~\n")
            gameBoard.printMap()
            coordinates = gameBoard.getFieldCoordinates()
            while (!isFilled) {
                if (gameBoard.checkShipPlace(coordinates[1], coordinates[0])) {
                    fillOneField(coordinates)
                    isFilled = true
                } else {
                    println("\n ~~~~  Введенная координата уже занята. ~~~~ \n")
                    coordinates = gameBoard.getFieldCoordinates()
                }
            }
        }

        println("\n ~~~~ Размести на карте единственный двухпалубный корабль. ~~~~\n")
        isFilled = false
        var firstField = gameBoard.getFieldCoordinates()
        while (!isFilled) {
            if (gameBoard.checkShipPlace(firstField[1], firstField[0])) {
                fillOneField(firstField)
                isFilled = true
            } else {
                println("\n ~~~~  Введенная координата уже занята. ~~~~ \n")
                firstField = gameBoard.getFieldCoordinates()
            }
        }

        var secondField = gameBoard.getFieldCoordinates()
        var secondFieldIsCorrect = false
        isFilled = false
        while (!secondFieldIsCorrect || !isFilled) {
            if (gameBoard.checkSecondCoordinates(firstField, secondField)) {
                secondFieldIsCorrect = true
            } else {
                println("\nВторая координата двухпалубного корабля должна примыкать к первой!\n")
                secondField = gameBoard.getFieldCoordinates()
            }
            if (gameBoard.checkShipPlace(secondField[1], secondField[0])) {
                isFilled = true
            } else {
                println("\n ~~~~  Введенная координата уже занята. ~~~~ \n")
                secondField = gameBoard.getFieldCoordinates()
            }
        }
        if (secondFieldIsCorrect && isFilled) fillOneField(secondField)
    }

    fun makeMove(): Array<Int> {
        println("\n\n ~~~~ ${this.name}, твой ход. ~~~~\n")
        return arrayOf(gameBoard.getColumnCoordinate(), gameBoard.getLineCoordinate())
    }
}



