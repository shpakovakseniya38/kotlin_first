package homework3

fun main() {
    // Task 1

    val treasure = intArrayOf(1, 3, 5, 10, 20, 50, 100, 200, 500, 1000,
        1, 3, 5, 10, 20, 50, 100, 1, 3, 5, 10, 20, 50, 100, 500,
        1, 3, 5, 10, 20, 50, 100,
        1, 3, 5, 10, 20, 50, 100, 500)
    var captainPart = 0
    var crewPart = 0

    for (coin in treasure) {
        if (coin % 2 == 0) captainPart++ else crewPart++
    }

    println("Джо получает $captainPart монет, а его команда - $crewPart монет.")

    // Task 2

    val gradeBook = intArrayOf(2, 3, 4, 5, 2, 4, 5, 3, 4, 4, 5, 2, 3, 4, 5, 3, 4, 5, 2, 4, 5, 3, 4, 5, 4, 4, 4, 3)
    var gradeA = 0
    var gradeB = 0
    var gradeC = 0
    var gradeD = 0

    for (grade in gradeBook) {
        when (grade) {
            2 -> gradeD++
            3 -> gradeC++
            4 -> gradeB++
            5 -> gradeA++
        }
    }

    println("Отличников - ${(100 * gradeA / gradeBook.size).toDouble()}%")
    println("Хорошистов - ${(100 * gradeB / gradeBook.size).toDouble()}%")
    println("Троечников - ${(100 * gradeC / gradeBook.size).toDouble()}%")
    println("Двоечников - ${(100 * gradeD / gradeBook.size).toDouble()}%")

    // Task 3

    val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

    for (i in 1 until myArray.size) {
        var isSwapped = false
        for (j in 0 until myArray.size - i) {
            if (myArray[j] > myArray[j + 1]) {
                val swap = myArray[j]
                myArray[j] = myArray[j + 1]
                myArray[j + 1] = swap
                isSwapped = true
            }
        }
        if (!isSwapped) break
    }

    for (item in myArray) {
        println(item)
    }
}
