package homework5_7

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Homework5JUnitKtTest {

    @Test
    fun getSum() {
        assertEquals(5.0, sum(2.0, 3.0))
    }

    @Test
    fun getSubtraction() {
        assertEquals(-11.0, subtraction(2.0, 13.0))
    }

    @Test
    fun getMultiply() {
        assertEquals(896.0, multiply(28.0, 32.0))
    }

    @Test
    fun getDivision() {
        assertEquals(8.2, division(41.0, 5.0))
        assertThrows(UnsupportedOperationException::class.java) { division(10.0, 0.0) }
    }

    @Test
    fun getCalculationMethod() {
        assertEquals(sum, getCalculationMethod("+"))
        assertEquals(subtraction, getCalculationMethod("-"))
        assertEquals(multiply, getCalculationMethod("*"))
        assertEquals(division, getCalculationMethod("/"))
        assertThrows(UnsupportedOperationException::class.java) { getCalculationMethod("%") }
    }
}