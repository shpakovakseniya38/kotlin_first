package homework5

// Task 1

abstract class Animal(private val name: String, var height: Int, var weight: Int) {
    private var satiety: Int = 0
    abstract var foodPreferences: Array<String>

    private fun checkFood(food: String): Boolean {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                break
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
        return correctFood
    }

    fun eat(food: String) {
        if (this.satiety >= 2) {
            return
        } else if (checkFood(food)) {
            this.satiety++
            println("${this.name} ест $food. Сытость: $satiety")
            if (this.satiety >= 2) {
                println("${this.name} наелся!")
                return
            }
        } else return
    }
}

class Lion (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("мясо", "рыба")
}


class Tiger (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("мясо", "рыба")
}

class Hippopotamus (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("листья", "овощи", "фрукты")
}

class Wolf (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("мясо", "рыба")
}

class Giraffe (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("листья", "овощи", "фрукты")
}

class Elephant (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("листья", "овощи", "фрукты")
}

class Chimpanzee (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("мясо", "рыба", "овощи", "фрукты")
}

class Gorilla (name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override var foodPreferences = arrayOf("овощи", "фрукты")
}

fun main() {
    // Test for task 1

    val gorilla = Gorilla("Маша", 180, 90)
    val chimpanzee = Chimpanzee("Жорж", 120, 40)
    val elephant = Elephant("Миша", 300, 300)
    val giraffe = Giraffe("Элла", 300, 180)
    val wolf = Wolf("Аркадий", 130, 80)
    val tiger = Tiger("Сережа", 120, 100)
    val hippopotamus = Hippopotamus("Бэлла", 150, 280)
    val lion = Lion("Анастасия", 130, 90)

    gorilla.eat("листья")
    gorilla.eat("овощи")
    gorilla.eat("фрукты")

    chimpanzee.eat("фрукты")
    elephant.eat("рыба")
    giraffe.eat("фрукты")
    wolf.eat("овощи")
    tiger.eat("рыба")
    hippopotamus.eat("листья")
    lion.eat("мясо")


    // Test for task 2
    val animals = arrayOf(gorilla, chimpanzee, elephant, giraffe, wolf, tiger, hippopotamus, lion)
    val food = arrayOf("листья", "овощи", "фрукты", "рыба", "мясо", "листья", "овощи", "фрукты", "рыба", "мясо")

    feedZoo(animals, food)
}

// Task 2
fun feedZoo(animals: Array<Animal>, food: Array<String>) {
    //some code
    for (animal in animals) {
        for (item in food) {
            animal.eat(item)
        }
    }
}


