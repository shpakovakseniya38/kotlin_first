package personDataGenerator

import com.google.gson.GsonBuilder
import com.itextpdf.text.BaseColor
import com.itextpdf.text.Document
import com.itextpdf.text.Phrase
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import java.io.File
import java.io.FileOutputStream
import java.util.*
import com.itextpdf.text.Font
import com.itextpdf.text.PageSize
import com.itextpdf.text.pdf.BaseFont

val FONT_PATH = "Arial Unicode.ttf"
val BASE_FONT = BaseFont.createFont(FONT_PATH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED)
fun main() {

    val scanner = Scanner(System.`in`)

        println("Введите количество людей (от 0 до 30): ")
    var n = scanner.nextInt()

    while (n !in 0..30) {
        println("Bведите число от 0 до 30: ")
        n = scanner.nextInt()
    }

    val people = List(n) { PersonGenerator(GenderDataGenerator().generate(), AddressGenerator().generate()).generate() }

    JsonFileCreator(people).createFile()
    PDFFileCreator(people).createFile()

}

interface RandomDataGenerator<T> {
    fun generate() : T
}

class TextGenerator(private val data: List<String>) : RandomDataGenerator<String> {
    override fun generate() : String {
        return data.random()
    }
}

class NumberGenerator(private val limit: Int) : RandomDataGenerator<Int> {
    override fun generate() : Int {
        val random = Random()
        return random.nextInt(limit)
    }
}

class PersonGenerator(genderData: GenderData, private val address: Address) : RandomDataGenerator<Person> {
    private val gender = if (genderData.gender) "м" else "ж"
    private val name = TextGenerator(genderData.names).generate()
    private val surname = TextGenerator(genderData.surnames).generate()
    private val patronymic = TextGenerator(genderData.patronymics).generate()
    private val birthPlace = CityGenerator().generate()
    private val yearOfBirth = 1930 + NumberGenerator(94).generate()
    private val dateOfBirth =
        "${NumberGenerator(28).generate()}-${NumberGenerator(12).generate()}-${yearOfBirth}"
    private val age = 2024 - yearOfBirth
    override fun generate() : Person {
        return Person(name, surname, patronymic, gender, age, dateOfBirth, birthPlace, address)
    }
}

class GenderDataGenerator : RandomDataGenerator<GenderData>{
    private val random = Random()
    private val genderFlag = random.nextBoolean()
    private val names = if (genderFlag) listOf("Иван", "Александр", "Петр", "Дмитрий", "Сергей") else listOf("Анна", "Мария", "Екатерина", "Ольга", "Ирина")
    private val surnames = if (genderFlag) listOf("Иванов", "Петров", "Сидоров", "Козлов", "Смирнов") else listOf("Иванова", "Петрова", "Сидорова", "Козлова", "Смирнова")
    private val patronymics = if (genderFlag) listOf("Иванович", "Петрович", "Александрович", "Дмитриевич", "Сергеевич") else listOf("Ивановна", "Петровна", "Александровна", "Егоровна", "Петровна")

    override fun generate() : GenderData {
        return GenderData(names, surnames, patronymics, genderFlag)
    }
}

class AddressGenerator : RandomDataGenerator<Address> {
    private val postalCode = String.format("%06d", NumberGenerator(1000000).generate())
    private val country = "Россия"
    private val region = TextGenerator(listOf("Московская область", "Ленинградская область", "Татарстан", "Свердловская область")).generate()
    private val city = CityGenerator().generate()
    private val street = TextGenerator(listOf("Ленина", "Пушкинская", "Гагарина")).generate()
    private val house = (NumberGenerator(100).generate() + 1).toString()
    private val apartment = (NumberGenerator(100).generate() + 1).toString()

    override fun generate() : Address {
        return Address(postalCode, country, region, city, street, house, apartment)
    }
}

class CityGenerator : RandomDataGenerator<String> {
    override fun generate() : String {
        return TextGenerator(listOf("Москва", "Красноярск", "Санкт-Петербург", "Казань", "Новосибирск", "Екатеринбург", "Нижний Новгород")).generate()
    }
}

interface FileCreator {
    fun createFile()
}

class PDFFileCreator(private val people: List<Person>) : FileCreator {
    override fun createFile() {
        val document = Document()
        PdfWriter.getInstance(document, FileOutputStream("people.pdf"))

        document.open()

        document.pageSize = PageSize.A4.rotate()
        val table = PdfPTable(14)

        addTableHeader(table)
        addRows(table, people)

        document.add(table)
        document.close()

        println("PDF файл успешно создан")
    }

    private fun addTableHeader(table: PdfPTable) {
        val headerTitles = listOf(
            "Имя",
            "Фамилия",
            "Отчество",
            "Возраст",
            "Пол",
            "Дата рождения",
            "Место рождения",
            "Индекс",
            "Страна",
            "Область",
            "Город",
            "Улица",
            "Дом",
            "Квартира"
        )
        repeat(headerTitles.size) { index ->
            val cell = createCell(headerTitles[index])
            table.addCell(cell)
        }
    }

    fun createCell(text: String): PdfPCell {
        val cell = PdfPCell()
        cell.backgroundColor = BaseColor.WHITE
        cell.borderWidth = 0.5f
        cell.setPhrase(Phrase(text, Font(BASE_FONT, 4f)))
        return cell
    }

    private fun addRows(table: PdfPTable, people: List<Person>) {
        repeat(people.size) { index ->
            table.addCell(createCell(people[index].name))
            table.addCell(createCell(people[index].surname))
            table.addCell(createCell(people[index].patronymic))
            table.addCell(createCell(people[index].age.toString()))
            table.addCell(createCell(people[index].gender))
            table.addCell(createCell(people[index].dateOfBirth))
            table.addCell(createCell(people[index].birthPlace))
            table.addCell(createCell(people[index].address.postalCode))
            table.addCell(createCell(people[index].address.country))
            table.addCell(createCell(people[index].address.region))
            table.addCell(createCell(people[index].address.city))
            table.addCell(createCell(people[index].address.street))
            table.addCell(createCell(people[index].address.house))
            table.addCell(createCell(people[index].address.apartment))
        }
    }
}

class JsonFileCreator(private val people: List<Person>) : FileCreator {
    override fun createFile(){
        val gson = GsonBuilder().setPrettyPrinting().create()
        val jsonContent = gson.toJson(people)
        val jsonFile = File("people.json")
        jsonFile.writeText(jsonContent)

        println("JSON файл успешно создан в директории: ${jsonFile.absolutePath}")
    }
}

data class Address (
    val postalCode: String,
    val country: String,
    val region: String,
    val city: String,
    val street: String,
    val house: String,
    val apartment: String
)

data class Person (
    val name: String,
    val surname: String,
    val patronymic: String,
    val gender: String,
    val age: Int,
    val dateOfBirth: String,
    val birthPlace: String,
    val address: Address
)

data class GenderData (
    val names: List<String>,
    val surnames: List<String>,
    val patronymics: List<String>,
    val gender: Boolean,
)