package homework2

// Task 1
fun main() {
    val whiskey: Byte = 50
    val pinaColada: Short = 200
    val lemonade: Int = 18500
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val ale: Double = 0.666666667
    val somethingOriginal: String = "Что-то авторское!"

// Task 2

    println("Заказ - \'$lemonade мл лимонада\' готов!")
    println("Заказ - \'$pinaColada мл пина колады\' готов!")
    println("Заказ - \'$whiskey мл виски\' готов!")
    println("Заказ - \'$fresh капель фреша\' готов!")
    println("Заказ - \'$cola литра колы\' готов!")
    println("Заказ - \'$ale литра эля\' готов!")
    println("Заказ - \"$somethingOriginal\" готов!")

}
