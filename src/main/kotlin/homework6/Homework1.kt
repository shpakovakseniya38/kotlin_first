package homework6

fun main() {

    val userOfHealthyMan = createUser("qwerty", "1234567890", "1234567890")
    println(userOfHealthyMan.login)

    //userLongLogin
    createUser("qwertyqwertyqwertyqwerty", "1234567890", "1234567890")

    //userLazyPassword
    createUser("user", "1", "1")

    // userFailCtrlC
    createUser("arkadiy", "arkadiy123", "nearkadiy890")
}

fun createUser(login: String, password: String, confirmPassword: String): User {
    if (login.length > 20) throw WrongLoginException("В логине более 20 символов!")

    if (password.length < 10) throw WrongPasswordException("В пароле менее 10 символов!")

    if (password != confirmPassword) throw WrongPasswordException("Пароль и подтверждение пароля не совпадают!")

    return User(login, password)
}

class WrongLoginException(message: String) : Exception(message)
class WrongPasswordException(message: String) : Exception(message)

class User(val login: String, val password: String)
