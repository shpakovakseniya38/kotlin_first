package homework7

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    //call your function here
    println(removeDuplicates(sourceList))
}

//write your code here
fun removeDuplicates(list: MutableList<Int>): List<Int> {
    return list.toSet().toList()
}