package homework7

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Количество овощей в корзине: ${countVegetables(userCart)}")
    println("Суммарная стоимость товаров в корзине: ${calculateCost(userCart)}")
}

fun countVegetables(cart: MutableMap<String, Int>): Int {
    var count = 0
    for (item in vegetableSet) {
        if (cart.contains(item)) {
            count += cart[item] ?: 0
        }
    }
    return count
}

fun calculateCost(cart: MutableMap<String, Int>): Double {
    var cost = 0.0
    for ((key, value) in cart) {
        val price = prices[key] ?: 0.0
        cost += if(discountSet.contains(key)) {
            price * value * (1 - discountValue)
        } else {
            price * value
        }
    }
    return cost
}