package homework4

class Lion (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("мясо", "рыба")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}


class Tiger (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("мясо", "рыба")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}

class Hippopotamus (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("листья", "овощи", "фрукты")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}

class Wolf (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("мясо", "рыба")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}

class Giraffe (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("листья", "овощи", "фрукты")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}

class Elephant (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("листья", "овощи", "фрукты")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}

class Chimpanzee (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("мясо", "рыба", "овощи", "фрукты")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}

class Gorilla (val name: String, var height: Int, var weight: Int) {
    private var foodPreferences = arrayOf("овощи", "фрукты")
    private var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, foodPreferences: Array<String>, satiety: Int) : this(name, height, weight) {
        this.foodPreferences = foodPreferences
        this.satiety = satiety
    }

    fun eat(food: String) {
        var correctFood = false
        for (item in this.foodPreferences) {
            if (item == food) {
                correctFood = true
                this.satiety++
                println("${this.name} ест $food. Сытость: $satiety")
                if (this.satiety >= 2) {
                    println("${this.name} наелся!")
                    break
                }
            }
        }
        if (!correctFood) println("${this.name} не ест $food!")
    }
}

fun main() {
    val gorilla = Gorilla("Маша", 180, 90)
    val chimpanzee = Chimpanzee("Жорж", 120, 40)
    val elephant = Elephant("Миша", 300, 300)
    val giraffe = Giraffe("Элла", 300, 180)
    val wolf = Wolf("Аркадий", 130, 80)
    val tiger = Tiger("Сережа", 120, 100)
    val hippopotamus = Hippopotamus("Бэлла", 150, 280)
    val lion = Lion("Анастасия", 130, 90)

    gorilla.eat("листья")
    gorilla.eat("овощи")
    gorilla.eat("фрукты")

    chimpanzee.eat("фрукты")
    elephant.eat("рыба")
    giraffe.eat("фрукты")
    wolf.eat("овощи")
    tiger.eat("рыба")
    hippopotamus.eat("листья")
    lion.eat("мясо")

}
