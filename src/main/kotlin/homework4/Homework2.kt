package homework4

fun main() {
    println("Введите целое число больше одного знака: \n")
    val digit = readLine()!!.toInt()
    println("Перевернутое число: ${reverseNumber(digit)}")
}

fun reverseNumber(number: Int): Int {
    var num = number
    var reversed = 0
    while (num != 0) {
        val digit = num % 10
        reversed = reversed * 10 + digit
        num /= 10
    }
    return reversed
}